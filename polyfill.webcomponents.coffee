###
Web Component Polyfill

Copyright 2011-2012 Christian Dannie Storgaard.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
###

window.__components = {}
window.__components_to_load = 0

bind = (element, event, func) ->
  if element.addEventListener? then (element.addEventListener event, func, false) else (
    if element.attachEvent?    then (element.attachEvent event, func) else null
  )
trigger = (element, event) ->
  if element.dispatchEvent? then (element.dispatchEvent event) else (
    if element.fireEvent?   then (element.fireEvent event) else null
  )

getAJAX = (url, callback_success, callback_error, type, data) ->
  XmlHttpObj = if window.XMLHttpRequest? then new window.XMLHttpRequest() else (
    if window.ActiveXObject? then (
      new window.ActiveXObject('MsXml2.XmlHttp')
    ) or (
      new window.ActiveXObject('Microsoft.XMLHTTP')
    ) else
      null
  )

  type = if type? then type.toUpperCase() else 'GET'
  data ?= null

  XmlHttpObj.onreadystatechange = XmlHttpObj.OnReadyStateChange = ->
    if XmlHttpObj.readyState is 4
      if XmlHttpObj.status is 200 or XmlHttpObj.status is 304
        #console.log 'Success'
        callback_success? url, XmlHttpObj.responseText
      else
        (callback_error? url, XmlHttpObj.statusText, XmlHttpObj.status) or (console.debug "XML request error: #{XmlHttpObj.statusText} (#{XmlHttpObj.status})")

  XmlHttpObj.open type, url, true
  if data and type is 'POST'
    XmlHttpObj.setRequestHeader 'Content-type', 'application/x-www-form-urlencoded'
  XmlHttpObj.send data
  #console.log "XmlHttpObj.open(#{type}, #{url}, true)"
  #console.log "XmlHttpObj.send(#{data})"
  #console.log XmlHttpObj

insertStyleSheet = (rules) ->
  # Create a new stylesheet
  style = window.document.createElement 'style'
  (window.document.getElementsByTagName 'head')[0].appendChild style

  # Safari does not see the new stylesheet unless you append something.
  # IE however will break on this, hence the test
  if not window.createPopup?
     style.appendChild window.document.createTextNode ''

  style = window.document.styleSheets[window.document.styleSheets.length - 1];

  for selector, text of rules
    if style.insertRule?
      style.insertRule selector + text, (style.cssRules?.length or style.rules?.length or 0)
    else if style.addRule?
      style.addRule selector, text.match(/{\s*(.*?)\s*}/m)[1]

insertScript = (script_data) ->
  # Create a new script
  script = document.createElement("script")
  script.type = "text/javascript"

  head = (window.document.getElementsByTagName 'head')[0]

  try
    # doesn't work on ie...
    script.appendChild document.createTextNode(script_data)
  catch err
    # IE has funky script nodes
    script.text = script_data

  head.insertBefore script, head.firstChild
  head.removeChild script
  #(window.document.getElementsByTagName 'head')[0].appendChild script


window.getComponents = () ->
  #console and console.log 'Initiated'
  components = []
  window.__components_to_load = 0
  for link in window.document.getElementsByTagName 'link'
    if link.rel? and link.rel is 'components' and link.href not in ( k for k,v of window.__components )
      components.push link.href
      window.__components_to_load += 1

  # If all component resources were already loaded, start parsing them
  if window.__components_to_load is 0
    window.componentsLoaded()
  else
    for component_url in components
      #$.get component_url, (data) =>
      getAJAX component_url, (url, data) =>
        window.__components[url] = data
        # Is everything loaded?
        if window.__components_to_load is components.length
          window.componentsLoaded()

###window.getAllChildren = (element, test) ->
  children = []
  if element.tagName? and (if test? then (test(element) is true) else true)
      children.push element
  if element.childNodes and element.childNodes.length
    for child in element.childNodes
      children = [].concat children, getAllChildren child, test
  return children

window.getInsertions = (element) ->
 window.getAllChildren element, (element) ->
    element.hasAttribute 'is'###
getInsertions = (element) ->
  element.querySelectorAll '[is]'

window.getComponentMarkup = (component, id) ->
  element = (component.match ///
    <element[\s\S]*?name=["']?#{id}["']?[\s\S]*?>
    ([\s\S]*?)
    </element>
  ///im)?[1] or ''

  template = (element.match ///
    <template>
    ([\s\S]*?)
    </template>
  ///im)?[1] or ''

  script = (element.match ///
    <script>
    ([\s\S]*?)
    </script>
  ///im)?[1] or ''

  style = (
    template.match ///
      [\s\n]*?(<style[\s\S]*?</style>)[\s\n]*?
    ///m
  )?[1] or ''

  html = template.replace ///
    [\s\n]*?<style[\s\S]*?</style>[\s\n]*?
  ///m, ''

  {
    'id'    : id
    'style' : style
    'html'  : html
    'script': script
  }


window.fillTemplateFromComponent = (template, dom) ->

  component_id = ((new Date).valueOf()+99)+''

  re = ///
    (
      <content.*?/content>
    |
      <content\s+.*?/>
    |
      <content\s*?/>
    )
  ///mg
  template_elements = []
  template_elements_important = []  # we use a separate array since template_elements will be sorted by length
  template_elements_mapper = {}
  template_elements_selectors = {}
  unique = 0
  while match = re.exec template.html+'' # operate on a copy
    #template_elements.push match[1]
    template_element = match[1]

    # Create a unique id for the place holder
    id = ((new Date).valueOf()+unique)+''

    # Add an 'added' attribute to the content element, so we don't get stuck in an infinite replacement loop
    template_element_new = template_element.replace /(<content)(.)/, '$1 added$2'
    template_elements_mapper[template_element_new] = "content_holder_#{id}"

    # Get the selector from the content element
    selector = (template_element.match /select[\s\n]*=[\s\n]*('[^']+'|"[^"]+"|[^\s\n]+)/m)?[1] or false
    # Remove any quotes around the selector
    selector = if selector and ( selector[0] is "'" or selector[0] is '"' ) then selector.slice(1, selector.length-1) else selector

    # Add a placeholder element after the content element
    template.html = template.html.replace template_element, "<span id=\"content_holder_#{id}\"></span>"

    # Add selector to dict
    template_elements_selectors[template_element_new] = selector
    # Add content element to either the "important" list (ID selectors)
    if selector[0] is '#'
      template_elements_important.push template_element_new
    # or the normal
    else
      template_elements.push template_element_new

    unique += 1

  # Sort reversed
  template_elements.sort (a, b) ->
    if a.length > b.length then -1 else if a.length < b.length then 1 else 0
  # Add the "important" selectors first, then the rest
  template_elements = [].concat template_elements_important, template_elements

  # We are now done with parsing the template
  # It's time to create a template element
  # and add the elements from the original DOM to it
  template_holder = document.createElement 'div'
  template_holder.setAttribute 'id', "component_#{component_id}"
  dom.parentNode.insertBefore template_holder, dom
  template_holder.innerHTML = template.html

  #console.log 'DOM:', dom

  for template_element in template_elements
    # Where to place the selected elements (just after the original content element)
    placeholder = template_holder.querySelector '#'+template_elements_mapper[template_element]

    if not placeholder?
      #console and console.log template_elements_mapper[template_element] + ' does not exist, skipping.'
    else
      # Get the children of the original element that match the selector
      if template_elements_selectors[template_element]
        selected_elements = dom.querySelectorAll template_elements_selectors[template_element]
      else
        selected_elements = ( i for i in dom.childNodes ) # create new array
      #console.log "Found by selector \"#{template_elements_selectors[template_element]  }\"", selected_elements

      # Go through them and add them just before the placeholder object
      for selected_element in selected_elements
        # If element still exist where it did originally
        if selected_element
          placeholder.parentNode.insertBefore selected_element, placeholder
        else
          # pass

      # Remove placeholder
      placeholder.parentNode.removeChild placeholder
  # Remove original element
  dom.parentNode.removeChild dom

  styles = {}
  selector_re = ///
    (
      [^\s\n}].*? # selector
    )
    \s*           # optional space
    (
      \{          # beginning of rules
      [\s\S]*?    # rules
      \}          # end of rules
    )
  ///gm
  style = (template.style.match />([\s\S]*?)<\//)?[1] or ''
  if style
    while match = selector_re.exec style
      styles["#component_#{component_id} #{match[1]}"] = match[2].replace /[\n\s]+/g, ' '
    insertStyleSheet styles

  if template.script.length
    insertScript template.script

window.componentsLoaded = () ->
  #console.log 'Set up components'
  for insertion in getInsertions document.body
    #console.log '\tSetting up: '+insertion.getAttribute 'is'
    component_name = insertion.getAttribute 'is'
    for url, component_resource of window.__components
      window._r = component_resource
      component_resource_lowered = component_resource.toLowerCase()
      #console.log '\t\tChecking if '+url+' contains "'+component_name+'"...'
      if component_resource_lowered.indexOf('name="'+component_name) isnt -1 or component_resource_lowered.indexOf('name=\''+component_name) isnt -1 or component_resource_lowered.indexOf('name='+component_name) isnt -1
        #console.log '\t\tIt does.'
        template = getComponentMarkup component_resource, component_name
        fillTemplateFromComponent template, insertion
        break
  window.$? and ($ window).trigger 'WebComponentsLoaded'

getComponents()
